<?php

function notificationapi_ui_user_overview($form_state, $account) {
  $form = array('#account' => $account);

  // TODO Settings variable for feature parity with notifications.
  $send_intervals = array(
    // -1 => t('Never'),
    0 => t('Immediately'),
    3600 => t('Every hour'),
    43200 => t('Twice a day'),
    86400 => t('Daily'),
    604800 => t('Weekly'),
  );

  $default_value = notificationapi_user_settings_get('send_interval', $account);
  $form['send_interval'] = array(
    '#type' => 'select',
    '#title' => t('Default send interval'),
    '#options' => $send_intervals,
    '#default_value' => $default_value,
    '#disabled' => count($send_intervals) == 1,
    '#description' => t('Default send interval for subscriptions.'),
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function notificationapi_ui_user_overview_submit($form, &$form_state) {
  foreach (element_children($form) as $setting) {
    if (FALSE !== array_search($setting, array('save', 'form_build_id', 'form_token', 'form_id'))) continue;
    notificationapi_user_settings_save($setting, $form_state['values'][$setting], $form['#account']);
  }
}

function notificationapi_subscription_user_subscriptions() {
  $form = array();

  return $form;
}
