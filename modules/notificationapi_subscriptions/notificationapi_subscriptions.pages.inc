<?php


function notificationapi_subscription_user_overview() {
  $form = array();

  $send_intervals = array();
//  $default_value = notifications_user_setting('send_interval', $form['_account']['#value']);
  $form['notificationapi_subscription_send_interval'] = array(
    '#type' => 'select',
    '#title' => t('Default send interval'),
    '#options' => $send_intervals,
    '#default_value' => array(),
    '#disabled' => count($send_intervals) == 1,
    '#description' => t('Default send interval for subscriptions.'),
  );

  return $form;
}

function notificationapi_subscription_user_subscriptions() {
  $form = array();

  return $form;
}
