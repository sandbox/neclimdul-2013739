<?php

use Drupal\notificationapi\Subscriptions;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Save a subscription object.
 *
 * @param object $subscription
 *   A stdClass representing a subscription.
 *
 * @return NULL
 */
function notificationapi_subscriptions_save($subscription) {

  $subscription = (object) $subscription;

  if ($subscription->sid) {
    $update = array('sid');
  }
  else {
    $update = array();
  }

  drupal_write_record('notificationapi_subscriptions', $subscription, $update);

  foreach ($subscription->conditions as $condition) {
    notificationapi_subscriptions_save_condition($subscription, $condition);
  }
}

/**
 * Save a condition object.
 *
 * @param object $subscription
 *   A stdClass representing a subscription.
 * @param object $condition
 *   A stdClass representing a condition.
 *
 * @return NULL
 */
function notificationapi_subscriptions_save_condition($subscription, $condition) {
  $subscription = (object) $subscription;
  $condition = (object) $condition;

  if ($condition->scid) {
    $update = array('scid');
  }
  else {
    $update = array();
  }

  // Make sure the condition sid is set correctly.
  $condition->sid = $subscription->sid;

  drupal_write_record('notificationapi_subscriptions_conditions', $condition, $update);
}

/**
 * Delete a subscription object.
 *
 * @para mixed $subscription
 *   Either the subscription id or subscription object to be deleted.
 */
function notificationapi_subscriptions_delete($subscription) {
  if (is_numeric($subscription)) {
    $sid = $subscription;
  }
  else {
    $subscription = (object) $subscription;
    $sid = $subscription->sid;
  }

  if ($sid) {
    db_query('DELETE FROM {notificationapi_subscriptions} WHERE sid = %d', $sid);
    db_query('DELETE FROM {notificationapi_subscriptions_conditions} WHERE sid = %d', $sid);
  }
}

/**
 * Load a list of subscriptions given a set of conditions.
 *
 * Accepted conditions include:
 *   - account: The user account associated with the subscriptions.
 *   - type: The type of even associated with the subscription.
 *     i.e. node.insert
 *
 * Note: I usually frown on weird query builders like this but its helpful
 * to generalize it like this because load_all means different things at
 * different points in our code.
 *
 * @param array $conditions
 *   Conditions used to build a query and filter results.
 *
 * @return array
 * An array of sobuscription objects and their conditions.
 */
function notificationapi_subscriptions_load_all($conditions = array()) {

  $query = 'SELECT * FROM {notificationapi_subscriptions}';

  $where = $args = array();
  if (isset($conditions['account'])) {
    $where[] = 'uid = %d';
    $args[] = $conditions['account']->uid;
  }

  if (isset($conditions['type'])) {
    $where[] = "event_type = '%s'";
    $args[] = $conditions['type'];
  }

  if (!empty($where)) {
    $query .= ' WHERE ' . implode(' AND ', $where);
  }

  $results = db_query($query, $args);
  $subscriptions = array();
  while ($subscription = db_fetch_object($results)) {
    $subscription->conditions = array();

    // TODO build a list of sids and aggregate this into one query?
    $conditions = db_query("SELECT * FROM {notificationapi_subscriptions_conditions} WHERE sid = %d", $subscription->sid);
    while ($condition = db_fetch_object($conditions)) {
      $condition->value = unserialize($condition->value);
      $subscription->conditions[] = $condition;
    }

    $subscriptions[] = $subscription;
  }

  return $subscriptions;
}

function notificationapi_subscriptions_notificationapi_dispatcher(EventDispatcherInterface $dispatcher) {
  $subscriber = new Subscriptions\SubscriptionSubscriber();
  $dispatcher->addSubscriber($subscriber);
}
