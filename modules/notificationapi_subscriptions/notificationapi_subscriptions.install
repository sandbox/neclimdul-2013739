<?php

function notificationapi_subscriptions_schema() {
  // Just stole this from notification.module for now. Don't know what the
  // fields are for or if they'll all get used.
  $schema['notificationapi_subscriptions'] = array(
    'description' => 'The base table for subscriptions',
    'fields' => array(
      'sid' => array(
        'description' => 'Unique subscription id',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'User id this subscription belongs to.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'event_type' => array(
        'description' => 'Type of event that triggers this subscription.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
      ),
      'send_method' => array(
        'description' => 'Sending method key.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE
      ),
      // TODO - Not used yet. Review and prune.
      'send_interval' => array(
        'description' => 'Sending interval for notifications of this subscription.',
        'type' => 'int', 'not null' => FALSE, 'disp-width' => '11'
      ),
      'cron' => array(
        'description' => '1 if this subscription will generate notifications to be processed on cron.',
        'type' => 'int', 'unsigned' => TRUE, 'size' => 'tiny', 'not null' => TRUE, 'default' => 0, 'disp-width' => '3'
      ),
      'status' => array(
        'description' => 'Subscription status: 0 = blocked, 1 = active, 2 = inactive',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
        'disp-width' => '11',
      ),
      'destination' => array(
        'description' => 'Alternate destination field for anonymous subscriptions, may be an email',
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('sid'),
  );

  $schema['notificationapi_subscriptions_conditions'] = array(
    'description' => 'The base table for subscriptions',
    'fields' => array(
      'scid' => array(
        'description' => 'Unique subscription condition id',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'sid' => array(
        'description' => 'Unique subscription id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      // This field is theoretically for removing dead conditions. I don't have
      // any code using it and once its confirmed necesarry this comment should
      // be removed.
      'type' => array(
        'description' => 'A general type for identifying where conditions come from.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
      ),
      'token' => array(
        'description' => 'A tokenized path down the context tree.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
      ),
      'value' => array(
        'description' => 'A value to be compared to the value in the context tree.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('scid'),
  );

  return $schema;
}


/**
 * Implementation of hook_install().
 */
function notificationapi_subscriptions_install() {
  drupal_install_schema('notificationapi_subscriptions');
}

/**
 * Implementation of hook_uninstall().
 */
function notificationapi_subscriptions_uninstall() {
  drupal_uninstall_schema('notificationapi_subscriptions');
  // TODO remove?
  foreach (array('events', 'send_intervals', 'sender', 'sendself', 'send_immediate') as $name) {
    variable_del("notifications_$name");
  }
}
