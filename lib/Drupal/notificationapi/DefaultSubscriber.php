<?php

namespace Drupal\notificationapi;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * A default subscriber for notification events.
 *
 * Doesn't really do anything ATM but shows what events exist and how to
 * catch them.
 */
class DefaultSubscriber implements EventSubscriberInterface {
  public static function getSubscribedEvents() {
    return array(
      NotificationEvents::RECIEVE => 'onRecieve',
      NotificationEvents::SUBSCRIPTIONS => 'onSubscriptions',
      NotificationEvents::DELIVER => 'onDeliver',
    );
  }

  public function onRecieve(Notifications\Event $e) {
  }

  public function onSubscriptions(Notifications\Event $e) {
  }

  public function onDeliver(Subscriptions\Event $e) {
    ctools_include('plugins');
    $class = ctools_plugin_load_class('notificationapi', 'messaging', $e->getSubscription()->getType(), 'class');
    if ($class) {
      $messaging_plugin = new $class($e);
      $messaging_plugin->deliver();
    }
  }
}
