<?php

/**
 *
 */

namespace Drupal\notificationapi;

/**
 * Events handled by the notification event system.
 */
final class NotificationEvents
{
    /**
     * A notification even has been recieved.
     *
     * This occurs before any processing has occurred.
     *
     * @var string
     */
    const RECIEVE = 'notification.recieve';

    /**
     * Subscriptions are being fetched for a notification.
     *
     * @var string
     */
    const SUBSCRIPTIONS = 'notification.subscriptions';

    /**
     * A notification subscription is being delivered.
     *
     * @var string
     */
    const DELIVER = 'notification.deliver';

}
