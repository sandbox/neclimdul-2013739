<?php

namespace Drupal\notificationapi\Subscriptions;

use Drupal\notificationapi\Notifications;
use Symfony\Component\EventDispatcher\Event as SymfonyEvent;

/**
 * Subscription event object.
 *
 * This Event is passed by subscription subscribers to delivery subscribers.
 */
class Event extends SymfonyEvent {

  protected $event;

  protected $subscription;

  public function __construct(Notifications\Event $event, Subscription $subscription) {
    $this->event = $event;
    $this->subscription = $subscription;
  }

  /**
   * Fetch current type information.
   */
  public function getType() {
    return $this->event->getType();
  }

  /**
   * Fetch current context information.
   */
  public function getContext() {
    return $this->event->getContext();
  }

  public function setSubscription(Subscription $subscription) {
    $this->subscription = $subscription;
  }

  public function getSubscription() {
    return $this->subscription;
  }
}
