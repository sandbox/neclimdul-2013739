<?php

namespace Drupal\notificationapi\Subscriptions;

use Drupal\notificationapi\Message;
use Drupal\notificationapi\NotificationEvents;
use Drupal\notificationapi\Notifications\Event as NotificationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * class SubscriptionSubscriber
 *
 * I'm not sure this will stay...
 */
class SubscriptionSubscriber implements EventSubscriberInterface {
  public static function getSubscribedEvents() {
    return array(
      NotificationEvents::SUBSCRIPTIONS => 'onSubscriptions',
    );
  }

  public function onSubscriptions(NotificationEvent $e) {

    $subscriptions = notificationapi_subscriptions_load_all(array('type' => $e->getType()));

    foreach ($subscriptions as $subscription) {
      $match = TRUE;
      foreach ($subscription->conditions as $condition) {
        $match &= $e->compareContextValue($condition->token, $condition->value);
      }

      if ($match) {
        // TODO more general message building. maybe based on the subscription type.
        $message = new Message(
          '[[ogname]] [title]',
          array(
            'body' => '[node-body]',
            'footer' => '[node-url]',
          ),
          $context['user'],
          NULL,
          $context);
        $subscription = new Subscription($e, $message, $subscription->send_method);
        $subscription_event = new Event($e, $subscription);
        $e->getDispatcher()->dispatch(NotificationEvents::DELIVER, $subscription_event);
      }
    }
  }
}
