<?php

namespace Drupal\notificationapi\Subscriptions;

use Drupal\notificationapi\Notifications\Event;
use Drupal\notificationapi\Message;

/**
 * Subscription event object.
 */
class Subscription {

  protected $event;

  protected $account;

  protected $type;

  protected $message;

  public function __construct(Event $event, Message $message, $type) {
    $this->event = $event;
    //dvm($event->getContext());
    $this->message = $message;
    $this->type = $type;
  }

  public function setType($type) {
    $this->type = $type;
  }

  public function getType() {
    return $this->type;
  }

  public function setAccount($account) {
    $this->account = $account;
  }

  public function getAccount() {
    return $this->account;
  }

  public function getMessage() {
    return $this->message;
  }
}
