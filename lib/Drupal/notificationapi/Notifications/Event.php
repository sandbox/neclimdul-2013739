<?php

namespace Drupal\notificationapi\Notifications;

use Symfony\Component\EventDispatcher\Event as SymfonyEvent;

/**
 * Notification event object.
 */
class Event extends SymfonyEvent {

  /**
   * @var
   * Context information
   */
  protected $context;

  /**
   * @var
   * Notification type.
   */
  protected $type;

  /**
   * @var
   * Whether the event has been handled.
   *
   * This has to do more with backgrounding and queueing then finishing.
   */
  protected $handled = FALSE;

  /**
   * Set the current context information.
   */
  public function setContext(array $context) {
    $this->context = $context;
  }

  /**
   * Fetch current context information.
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * Get a value from an object on the context object.
   *
   * @param string $token
   *   A colon seperated string representing the path through the object.
   */
  public function getContextValue($token) {

    // make sure we're provided a token value.
    if (empty($token)) return FALSE;

    $trail = explode(':', $token);

    $tmp = $this->getContext();

   foreach ($trail as $key) {
      if (is_array($tmp) && isset($tmp[$key])) {
        $tmp = $tmp[$key];
      }
      elseif (is_object($tmp) && isset($tmp->$key)) {
        $tmp = $tmp->$key;
      }
      else {
        return FALSE;
      }
    }

    return $tmp;
  }

  /**
   * Some generalized logic for looking for variables in context objects.

   * @param string $token
   *   A colon seperated string representing the path through the object.
   * @param mixed $value
   *   A value to be compared to the one stored at the path in the context object...
   * @return bool
   *   The boolean comparison of the provided value to the one at the provided location.
   */
  public function compareContextValue($token, $value) {
    $return = $this->getContextValue($token);
    if (is_array($return)) {
      // If the value is an array we're looking for a key match.
      if (is_array($value)) {
        return (bool) array_intersect_key($value, $return);
      }

      // Otherwise look for the value in the array.
      return (bool) array_search($value, $return);
    }

    // Fallback to the simple case of a straight comparison.
    return $value == $return;
  }

  /**
   * Set the current type information.
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Fetch current type information.
   */
  public function getType() {
    return $this->type;
  }

  /**
   *
   */
  public function isHandled($handled = NULL) {
    if (isset($handled)) $this->handled = $handled;
    return $this->handled;
  }
}
