<?php

namespace Drupal\notificationapi;

use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Notification something class.
 */
class Notification {

  protected $dispatcher;

  public function __construct($dispatcher) {
    $this->dispatcher = $dispatcher;
  }

  public static function Create() {
    $dispatcher = self::createDispatcher();

    return new self($dispatcher);
  }

  /**
   * ...
   */
  private static function createDispatcher() {
    $dispatcher = new EventDispatcher();
    module_invoke_all('notificationapi_dispatcher', $dispatcher);
    //foreach (module_implements('notificationapi_dispatcher') as $module) {
    //  $func = "${module}_notificationapi_dispatcher";
    //  $func($dispatcher);
    //}
    return $dispatcher;
  }


  public function process($type, $context) {
    $event = new Notifications\Event();
    $event->setContext($context);
    $event->setType($type);
    return $this->processEvent($event);
  }

  public function processEvent(Notifications\Event $event) {
    $this->dispatcher->dispatch(NotificationEvents::RECIEVE, $event);

    // Did something ask to stop processing?
    if ($event->isHandled()) {
      return;
    }

    $this->dispatcher->dispatch(NotificationEvents::SUBSCRIPTIONS, $event);
  }
}
