<?php

namespace Drupal\notificationapi;

/**
 * Message object.
 *
 * Simple message object for passing around messages for delivery.
 */
class Message {

  protected $subject;

  protected $body;
  protected $body_compiled = FALSE;

  protected $context;

  //protected $sender;
  //
  protected $recipient;

  function __construct($subject = NULL, $body = NULL, $recipient_account = NULL, $sender_account = NULL, $context = NULL) {
    $this->subject = $subject;
    $this->body = $body;
    $this->context = $context;
    $this->sender = $sender_account;
    $this->recipient = $recipient_account;
  }

  function getSubject() {
    return token_replace_multiple($this->subject, $this->context);
  }

  function setSubject($subject) {
    $this->subject = $subject;
  }

  /**
   * Build a message body for delivery.
   */
  function getBody() {
    if (!$this->body_compiled) {
      if (is_array($this->body)) {
        foreach ($this->body as $k => $body) {
          $this->body[$k] = token_replace_multiple($body, $this->context);
        }
      }
      else {
        $this->body = token_replace_multiple($this->body, $this->context);
      }

      $this->body_compiled = TRUE;
    }

    return $this->body;
  }

  function setBody($body) {
    $this->body = $body;
    $this->body_compiled = FALSE;
  }

  function getSender() {
    return $sender;
  }

  function setSender($sender) {
    $this->sender = $sender;
  }

  function getRecipient() {
    return $this->recipient;
  }

  function setRecipient($recipient) {
    $this->recipient = $recipient;
  }
}
