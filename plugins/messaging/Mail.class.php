<?php

use Drupal\notificationapi\Subscriptions\Event;

class Mail {

  protected $event;

  public function __construct(Event $event) {
    $this->event = $event;
  }

  public function deliver() {
    $message = $this->event->getSubscription()->getMessage();

    $recipient = $message->getRecipient();
    $to = $recipient->name . " <$recipient->mail>";
    $language = user_preferred_language($recipient);

    drupal_mail('notificationapi', 'notification', $to, $language, array(
      'message' => $message,
      'event' => $this->event,
      'subscription' => $this->event->getSubscription(),
    ));
  }
}
